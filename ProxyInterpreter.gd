extends "res://addons/scenario_editor/nodes/scenario_interpreter.gd"

export(bool) var enabled
export(bool) var signal_connected
export(NodePath) var Sub_interpreter
export(NodePath) var Proxy_interpreter

var sub_interpreter
var proxy_interpreter

func _ready():
	sub_interpreter = get_node(Sub_interpreter)
	_connect_interpreter(sub_interpreter)
	
	proxy_interpreter = get_node(Proxy_interpreter)
	if signal_connected:
		_connect_interpreter(proxy_interpreter)

func _connect_interpreter(interpreter):
	interpreter.connect("next", self, "_next")
	interpreter.connect("finished", self, "_finished")

func interpret(node):
	if enabled:
		proxy_interpreter.interpret(node)
	
	sub_interpreter.interpret(node)
