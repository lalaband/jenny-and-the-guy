tool
extends MarginContainer

export(String) var text = "" setget _text_set

signal pressed

onready var label = $Label

func _ready():
	_text_set(text)

func _text_set(new_value):
	if (label != null):
		label.text = new_value
	text = new_value

func _on_TextureButton_pressed():
	emit_signal('pressed')
