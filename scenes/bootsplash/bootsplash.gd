extends Control

const END_ANIMATION_POS = 6.66

func _input(event):
	if event.is_pressed():
		$AnimationPlayer.seek(END_ANIMATION_POS)

func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()
