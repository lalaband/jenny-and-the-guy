extends Control

signal answered(answer_index)

onready var description_label = $window/MarginContainer/VBoxContainer/ScrollContainer/description
onready var answers_container = $window/MarginContainer/VBoxContainer/answers
var answer_button = preload("res://gui/button.tscn")

func _ready():
	pass

# Met à jour l'humeur de l'interface.
#
# mood: La nouvelle humeur. Doit être un nombre entre 0 (triste) et 100 (content).
#
func update_mood(mood):
	for mood in [$"mood/0-20", $"mood/20-50", $"mood/50-60", $"mood/60-80", $"mood/80-100"]:
		mood.visible = false
	
	if mood > 80:
		$"mood/80-100".visible = true
	elif mood > 60:
		$"mood/60-80".visible = true
	elif mood > 50:
		$"mood/50-60".visible = true
	elif mood > 20:
		$"mood/20-50".visible = true
	else:
		$"mood/0-20".visible = true

# Met à jour le compteur d'argent de l'interface.
#
# money: Un nombre représentant l'argent.
#
func update_money(money):
	$money.text = "$" + String(money)

# Affiche la fenêtre contenant une longue description suivie de plusieurs boutons.
#
# description: Le texte a afficher dans la fenêtre.
# answers: Une liste de texte qui serra dans chaque boutons.
#
func show_window(description, answers):
	description_label.text = description
	
	for child in answers_container.get_children():
		answers_container.remove_child(child)
	
	for i in range(answers.size()):
		var button = answer_button.instance()
		button.text = answers[i]
		button.connect('pressed', self, '_on_button_pressed', [i])
		answers_container.add_child(button)
	
	$window_anim.play('open')

# Cache la fenêtre.
#
func hide_window():
	$window_anim.play('close')

func _on_button_pressed(answer_index):
	emit_signal('answered', answer_index)

func _on_Game_mood_changed(mood):
	update_mood(mood)

func _on_Game_money_changed(money):
	update_money(money)
