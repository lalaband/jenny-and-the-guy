extends Control

var money = 0
var mood = 0
var text_intro = ""
var stage_win_money = 0
var stage_lose_money = 0
var text_end_win = ""
var text_end_lose = ""
var final_question = ""

var questions = []
var questions_id = {}
var questions_random = []
var questions_pending = []
var available_questions = []

var question_opened = null

signal finished
signal mood_changed(mood)
signal money_changed(money)

enum State { SIMPLE_MESSAGE_OPENED, WAIT_NEXT_QUESTION, QUESTION_OPENED }
var state = null

onready var hud = $Hud
onready var next_question_timer = $next_question

func _ready():
	load_data()
	start_game()

func error(message):
	print(message)

func get_v(key, t):
	if not t.has(key):
		error("Miss: "+key)
	else:
		return t[key]

func load_data():
	var file = File.new()
	file.open("user://data.json", file.READ)
	if not file.is_open():
		file.open("/data.json", file.READ)
	if not file.is_open():
		file.open("res://data_FR.json", file.READ)

	var text = file.get_as_text()
	file.close()

	if text == "":
		print("texte vide!")
		return

	var data = parse_json(text)

	money = get_v("money", data)
	mood = get_v("mood", data)
	stage_win_money = get_v("win_money", data)
	stage_lose_money = get_v("lose_money", data)
	text_intro = get_v("intro", data)
	text_end_win = get_v("end_win", data)
	text_end_lose = get_v("end_lose", data)
	final_question = get_v("final_question", data)
	questions = get_v("questions", data)

	for i in range(questions.size()):
		var q = questions[i]
		for k in ["id", "description", "event"]:
			if not q.has(k):
				q[k] = ""
				print("Miss key ", k, " in the question n°", i)
		for k in ["availableTime", "preparationTime"]:
			if not q.has(k):
				q[k] = 0
				print("Miss key ", k, " in the question n°", i)
		if not q.has("random"):
			q["random"] = false
			print("Miss key ", "random", " in the question n°", i)
		if not q.has("answers"):
			q["answers"] = []
			print("Miss key ", "answers", " in the question n°", i)
		else:
			for a in q["answers"]:
				for k in ["label", "question", "comment"]:
					if not a.has(k):
						a[k] = ""
						print("Miss key ", k, " in the question n°", i)
				for k in ["money", "mood"]:
					if not a.has(k):
						a[k] = 0
						print("Miss key ", k, " in the question n°", i)

		if q["id"] != "":
			questions_id[q["id"]] = i
		if q["random"]:
			questions_random.append(i)
	
	available_questions = questions_random

func start_game():
	$AudioStreamPlayer.play()
	emit_signal("mood_changed", mood)
	emit_signal("money_changed", money)
	show_simple_message(text_intro)

func stop_game():
	$AudioStreamPlayer.stop()

func show_simple_message(message):
	change_state(SIMPLE_MESSAGE_OPENED)
	hud.show_window(message, ["Fermer"])

func _on_Hud_answered(answer_index):
	match state:
		SIMPLE_MESSAGE_OPENED:
			change_state(WAIT_NEXT_QUESTION)

		QUESTION_OPENED:
			var answer = question_opened["answers"][answer_index]

			mood = clamp(mood + answer["mood"], 0, 100)
			emit_signal("mood_changed", mood) # emit quand answer diff null
			money += answer["money"]
			emit_signal("money_changed", money)

			var next_question = answer["question"]
			var comment = answer["comment"]

			if next_question != "":
				questions_pending.append(questions[questions_id[next_question]])

			if comment != "":
				show_simple_message(comment)
			else:
				change_state(WAIT_NEXT_QUESTION)

func change_state(new_state):
	match state:
		SIMPLE_MESSAGE_OPENED:
			hud.hide_window()
		QUESTION_OPENED:
			hud.hide_window()

	state = new_state

	match state:
		WAIT_NEXT_QUESTION:
			next_question_timer.start()

func get_random_question():
	if available_questions.empty():
		return null

	var index_question = randi() % available_questions.size()
	var question = questions[available_questions[index_question]]
	available_questions.remove(index_question)

	return question

func get_pending_question():
	if questions_pending.empty():
		return null

	return questions_pending.pop_front()

func show_next_question():

	var question = get_pending_question()
	if question == null:
		question = get_random_question()
	if question == null:
		print("plus de question à afficher")
		return

	question_opened = question

	var answers = []
	for answer in question["answers"]:
		answers.append(answer["label"])

	hud.show_window(question["description"], answers)
	change_state(QUESTION_OPENED)

func _on_next_question_timeout():
	match state:
		WAIT_NEXT_QUESTION:
			show_next_question()
