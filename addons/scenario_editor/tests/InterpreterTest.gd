extends "../nodes/scenario_interpreter.gd"

const QUESTION = preload("res://addons/scenario_editor/resources/question.gd")
const MESSAGE = preload("res://addons/scenario_editor/resources/message.gd")
const START_SCENARIO = preload("res://addons/scenario_editor/resources/start_scenario.gd")
const STOP_SCENARIO = preload("res://addons/scenario_editor/resources/stop_scenario.gd")
const EVERY_ITERATION = preload("res://addons/scenario_editor/resources/every_iteration.gd")

export(NodePath) var Hud

var hud

func _ready():
	hud = get_node(Hud)
	hud.connect("answered", self, "_hud_answered")

func interpret(node):
	if node is MESSAGE:
		hud.show_window(node.text, ["Close"])
		
	if node is QUESTION:
		print("answers ", node.answers)
		hud.show_window(node.text, node.answers)
	
	if node is STOP_SCENARIO:
		hud.hide_window()
	
	.interpret(node)

func _hud_answered(port):
	_next(port)
