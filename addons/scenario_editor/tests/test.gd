tool
extends Node

#onready var player = $ScenarioPlayer

func _ready():
	print("__TEST BEGIN__")
	
#	var question = load("res://addons/scenario_editor/tests/question.gd").new()
#	question.offset = Vector2(10, 10)
#	question.text = "Une question"
#	question.answers = ["Rep 1", "Rep 2"]
#
#	var message = load("res://addons/scenario_editor/tests/message.gd").new()
#	question.offset = Vector2(30, 30)
#	message.text = "Du texte dans un message"
	
#	var start_scenario = load("res://addons/scenario_editor/tests/start_scenario.gd").new()
#	var stop_scenario = load("res://addons/scenario_editor/tests/stop_scenario.gd").new()
#	var every_iteration = load("res://addons/scenario_editor/tests/every_iteration.gd").new()
#
#	var scenario = load("res://addons/scenario_editor/tests/scenario.gd").new()
#	scenario.nodes.clear()
#	scenario.in_edges.clear()
#	scenario.out_edges.clear()
#
#	scenario.add_node(start_scenario)
#	scenario.add_node(stop_scenario)
#	scenario.add_node(every_iteration)
#
#	scenario.connect_node(start_scenario, 0, stop_scenario, 0)
#
#	ResourceSaver.save("res://addons/scenario_editor/tests/scenario.tres", scenario)
#
#	player.scenario = scenario
#
#	player.play()
#
	var node = load("res://addons/scenario_editor/editor/nodes/node.tscn").instance()
	var message = load("res://addons/scenario_editor/editor/nodes/message.tscn").instance()
#	message._init()

	$GraphEdit.add_child(node)
	$GraphEdit.add_child(message)

	print("__TEST END__")
