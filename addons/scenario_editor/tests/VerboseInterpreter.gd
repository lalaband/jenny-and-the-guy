extends "res://addons/scenario_editor/nodes/scenario_interpreter.gd"

func _ready():
	connect("next", self, "_print_next")

func interpret(node):
	print("node : ", node.resource_path)
	
	.interpret(node)

func _print_next(port):
	print("next : ", port)
