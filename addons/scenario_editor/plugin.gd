tool
extends EditorPlugin

const SCENARIO = preload("resources/scenario.gd")
const SCENARIO_PLAYER = preload("nodes/scenario_player.gd")

var scenario_editor = null
var toolbutton = null

func _enter_tree():
	scenario_editor = preload("editor/scenario_editor.tscn").instance()
	
	toolbutton = add_control_to_bottom_panel(scenario_editor, "Scenario Editor")
	toolbutton.visible = false
	
	add_custom_type("Scenario", "Resource", SCENARIO, null)
	add_custom_type("ScenarioPlayer", "Node", SCENARIO_PLAYER, null)

func _exit_tree():
	remove_control_from_bottom_panel(scenario_editor)
	
	scenario_editor.free()
	scenario_editor = null
	
	remove_custom_type("Scenario")
	remove_custom_type("ScenarioPlayer")

func apply_changes():
	print("apply changes")

func clear():
	print("clear")
	scenario_editor.scenario = null

func handles(object):
    return object is SCENARIO or object is SCENARIO_PLAYER

func make_visible(visible):
    toolbutton.visible = visible
    
    if visible:
        make_bottom_panel_item_visible(scenario_editor)
    else:
        hide_bottom_panel()

func edit(object):
    if object is SCENARIO:
        scenario_editor.scenario = object

    if object is SCENARIO_PLAYER:
        scenario_editor.scenario = object.scenario
    
func save_external_data():
	if scenario_editor != null and scenario_editor.scenario != null and scenario_editor.scenario.resource_path != "":
		ResourceSaver.save(scenario_editor.scenario.resource_path, scenario_editor.scenario)
