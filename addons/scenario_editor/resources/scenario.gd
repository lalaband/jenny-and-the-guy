tool
extends Resource

export(Array) var nodes = []
export(Resource) var start_scenario_node = null
export(Resource) var every_iteration_node = null

export var in_edges = {}
export var out_edges = {}

signal node_connected(from, from_port, to, to_port)
signal node_disconnected(from, from_port, to, to_port)

static func new():
	print("new scenario")
	var scenario = load("scenario.gd").new()
	var start_scenario = preload("start_scenario.gd").new()
	var stop_scenario = preload("stop_scenario.gd").new()
	var every_iteration = preload("every_iteration.gd").new()
	scenario.add_node(start_scenario)
	scenario.add_node(stop_scenario)
	scenario.add_node(every_iteration)
	scenario.connect_node(start_scenario, 0, stop_scenario, 0)
	
	return scenario

func add_node(node):
	nodes.append(node)
	in_edges[node] = {}
	out_edges[node] = {}
	
	if start_scenario_node == null and node is preload("start_scenario.gd"):
		start_scenario_node = node
	
	if every_iteration_node == null and node is preload("every_iteration.gd"):
		every_iteration_node = node

func remove_node(node):
	nodes.erase(node)

	var to = node
	for to_port in in_edges[to]:
		for from in in_edges[to][to_port]:
			var from_port = in_edges[to][to_port][from]
			out_edges[from].erase(from_port)
			emit_signal("node_disconnected", from, from_port, to, to_port)
	
	var from = node
	for from_port in out_edges[from]:
		var d = out_edges[from][from_port]
		in_edges[d.to][d.to_port].erase(from)
		emit_signal("node_disconnected", from, from_port, d.to, d.to_port)
	
	in_edges.erase(node)
	out_edges.erase(node)

func connect_node(from, from_port, to, to_port):
	if not in_edges[to].has(to_port):
		in_edges[to][to_port] = {}
	
	if out_edges[from].has(from_port):
		var last_to = out_edges[from][from_port]
		disconnect_node(from, from_port, last_to.to, last_to.to_port)
	
	in_edges[to][to_port][from] = from_port
	out_edges[from][from_port] = { to = to, to_port = to_port }
	
	emit_signal("node_connected", from, from_port, to, to_port)
	
func disconnect_node(from, from_port, to, to_port):
	in_edges[to][to_port].erase(from)
	out_edges[from].erase(from_port)
	
	emit_signal("node_disconnected", from, from_port, to, to_port)

func get_connection_list():
	var connections = []
	
	for from in out_edges:
		for from_port in out_edges[from]:
			var d = out_edges[from][from_port]
			connections.append({from = from, from_port = from_port, to = d.to, to_port = d.to_port})
	
	return connections
	
func get_connection_list_of(node):
	var connections = []
	
	var from = node
	for from_port in out_edges[from]:
		var d = out_edges[from][from_port]
		connections.append({from = from, from_port = from_port, to = d.to, to_port = d.to_port})

	
	print("FIRST")
	print(connections)
	
	var to = node
	for to_port in in_edges[to]:
		for from in in_edges[to][to_port]:
			var from_port = in_edges[to][to_port][from]
			connections.append({from = from, from_port = from_port, to = to, to_port = to_port})

	print("SECOND")
	print(connections)
	
	return connections
