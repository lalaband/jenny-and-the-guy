tool
extends HBoxContainer

var node
var answer_index

signal answer_removed()

func _ready():
	$LineEdit.text = node.answers[answer_index]

func _on_LineEdit_text_changed(new_text):
	print(answer_index, " : ", new_text)
	node.answers[answer_index] = new_text

func _on_CloseButton_pressed():
	node.answers.remove(answer_index)
	emit_signal("answer_removed")
	queue_free()
