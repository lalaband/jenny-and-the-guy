tool
extends GraphNode

const NodeResource = preload("../../resources/node.gd")

var node = null

signal slot_left_added(slot)
signal slot_right_added(slot)
signal slot_left_removed(slot)
signal slot_right_removed(slot)

func _init():
	node = NodeResource.new()

func _ready():
	offset = node.offset
	connect("offset_changed", self, "_on_offset_changed")

func _on_offset_changed():
	node.offset = offset
