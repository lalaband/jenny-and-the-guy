tool
extends "node.gd"

const NodeResource = preload("../../resources/message.gd")

func _ready():
	$TextEdit.text = node.text

func _on_TextEdit_text_changed():
	node.text = $TextEdit.text
