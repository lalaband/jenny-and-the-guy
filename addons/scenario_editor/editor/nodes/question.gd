tool
extends "node.gd"

const NodeResource = preload("../../resources/question.gd")
const Answer = preload("question_answer.tscn")

func _ready():
	$VBoxContainer/TextEdit.text = node.text
	
	for answer_index in range(node.answers.size()):
		_add_answer_editor(answer_index)

func _add_answer_editor(answer_index):
	var answer_editor = Answer.instance()
	answer_editor.node = node
	answer_editor.answer_index = answer_index
	answer_editor.connect("answer_removed", self, "_on_answer_deleted")
	add_child(answer_editor)
	
	var slot = get_child_count() - 1
	
	set_slot(slot, false, 0, Color(), true, 0, Color(1, 1, 1), null, null)
	
	emit_signal("slot_right_added", slot)

func _on_TextEdit_text_changed():
	node.text = $VBoxContainer/TextEdit.text

func _on_NewAnswer_pressed():
	var answer_index = node.answers.size()
	node.answers.append("")
	_add_answer(answer_index)

# faire la déconnection des trucs
func _on_answer_deleted():
	var i_index = 0
	var i_child = 1 # skip first ones
	var deleted_slot = -1
	
	while i_child < get_child_count():
		var c = get_child(i_child)
		i_child += 1
		
		if c.is_queued_for_deletion():
			deleted_slot = i_index
			continue
		
		c.answer_index = i_index
		i_index += 1
	
	print(deleted_slot)
	emit_signal("slot_right_removed", deleted_slot)
