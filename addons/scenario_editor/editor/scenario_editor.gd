tool
extends Control

var scenario setget _set_scenario

#Debuger
#var scenario_player = null
#var my_player = null
#var previous_node = null
#var current_node = null

onready var graph = $VBoxContainer/GraphEdit

var node_to_node_editor = {}

const node_editor_name_to_node_editor_class = {
	StartScenario = preload("nodes/start_scenario.tscn"),
	StopScenario = preload("nodes/stop_scenario.tscn"),
	EveryIteration = preload("nodes/every_iteration.tscn"),
	Question = preload("nodes/question.tscn"),
	Message = preload("nodes/message.tscn"),
}

var node_script_to_node_editor_class = {}

func _ready():
	for node_editor_name in node_editor_name_to_node_editor_class:
		var node_editor_class = node_editor_name_to_node_editor_class[node_editor_name]
		var node_editor = node_editor_class.instance()
		node_script_to_node_editor_class[node_editor.NodeResource] = node_editor_class
		node_editor.free()

func _set_scenario(new_scenario):
	if scenario == new_scenario:
		return
	
	graph.clear_connections()
	for c in graph.get_children():
		if c is GraphNode:
    		c.queue_free()
	
	if scenario != null:
		scenario.disconnect("node_connected", self, "_on_scenario_node_connected")
		scenario.disconnect("node_disconnected", self, "_on_scenario_node_disconnected")
	
	scenario = new_scenario
	
	if scenario == null:
		return
	
	scenario.connect("node_connected", self, "_on_scenario_node_connected")
	scenario.connect("node_disconnected", self, "_on_scenario_node_disconnected")
	
#	scenario_player.scenario = scenario
	
	for node in scenario.nodes:
		var node_editor = node_script_to_node_editor_class[node.get_script()].instance()
		node_editor.node = node
		_add_node_editor(node_editor)
	
	for c in scenario.get_connection_list():
		graph.connect_node(node_to_node_editor[c.from].name, c.from_port, node_to_node_editor[c.to].name, c.to_port)


func _add_new_node_editor(node_editor_name):
	var node_editor = node_editor_name_to_node_editor_class[node_editor_name].instance()
	_add_node_editor(node_editor)
	scenario.add_node(node_editor.node)

func _add_node_editor(node_editor):
	node_to_node_editor[node_editor.node] = node_editor
	graph.add_child(node_editor)
	node_editor.connect("close_request", self, "_on_node_close", [node_editor], CONNECT_ONESHOT)

func _node_editor_name_to_node(node_editor_name):
	return graph.get_node(node_editor_name).node

# Node

func _on_node_close(node_editor):
	scenario.remove_node(node_editor.node)
	node_editor.queue_free()
	node_to_node_editor.erase(node_editor.node)

# Toolbar

func _on_Load_pressed():
	_set_scenario(ResourceLoader.load("res://addons/scenario_editor/tests/scenario.tres"))

func _on_Save_pressed():
	ResourceSaver.save("res://addons/scenario_editor/tests/scenario.tres", scenario)

func _on_NewQuestion_pressed():
	_add_new_node_editor("Question")

func _on_NewMessage_pressed():
	_add_new_node_editor("Message")

# GraphEdit

func _on_GraphEdit_connection_request(from, from_slot, to, to_slot):
	scenario.connect_node(_node_editor_name_to_node(from), from_slot, _node_editor_name_to_node(to), to_slot)

func _on_GraphEdit_disconnection_request(from, from_slot, to, to_slot):
	scenario.disconnect_node(_node_editor_name_to_node(from), from_slot, _node_editor_name_to_node(to), to_slot)

func _on_GraphEdit_node_selected(node):
	pass

func _on_GraphEdit_popup_request(p_position):
	$VBoxContainer/GraphEdit/Popup.popup(Rect2(p_position, Vector2(50, 100)))

func _on_GraphEdit_connection_to_empty(from, from_slot, release_position):
	$VBoxContainer/GraphEdit/Popup.popup(Rect2(release_position, Vector2(50, 100)))
	# fait la node et la connecte
	pass # replace with function body

# Scenario

func _on_scenario_node_connected(from, from_port, to, to_port):
	graph.connect_node(node_to_node_editor[from].name, from_port, node_to_node_editor[to].name, to_port)

func _on_scenario_node_disconnected(from, from_port, to, to_port):
	graph.disconnect_node(node_to_node_editor[from].name, from_port, node_to_node_editor[to].name, to_port)


#func _ready():
#	my_player = Node.new()
#	add_child(my_player)
#	my_player.set_script(preload("res://addons/scenario_editor/nodes/scenario_player.gd"))
#	my_player.scenario = _empty_scenario()
#
#	_set_scenario_player(my_player)
#
#	_set_play_buttons(false)

#func _set_scenario_player(new_scenario_player):
#	scenario_player = new_scenario_player
#	scenario_player.connect("current_node_changed", self, "_on_scenario_player_current_node_changed")
#	scenario_player.connect("started", self, "_on_scenario_player_started")
#	scenario_player.connect("stoped", self, "_on_scenario_player_stoped")
#
#	_set_scenario(scenario_player.scenario)


#func _on_scenario_player_current_node_changed(node):
#	if current_node == node:
#		if node is MESSAGE:
#			scenario_player.next()
#		if node is QUESTION:
#			scenario_player.next()
#		return
#
#	scenario_player.pause()
#
#	if node is MESSAGE:
#		print("message : ", node.text)
##		scenario_player.next()
#	if node is QUESTION:
#		print("question : ", node.text)
##		scenario_player.next()
#
#	previous_node = current_node
#	current_node = node
#
#	if previous_node != null:
#		node_to_node[previous_node].overlay = GraphNode.OVERLAY_DISABLED
#
#	node_to_node[current_node].overlay = GraphNode.OVERLAY_BREAKPOINT

#func _on_scenario_player_started():
#	_set_play_buttons(true)

#func _on_scenario_player_stoped():
#	_set_play_buttons(false)
#	node_to_node[current_node].overlay = GraphNode.OVERLAY_DISABLED

#func _set_play_buttons(playing):
#	$VBoxContainer/HBoxContainer/Play.visible = not playing
#	$VBoxContainer/HBoxContainer/Next.visible = playing
#	$VBoxContainer/HBoxContainer/Stop.visible = playing

#func _on_Play_pressed():
#	scenario_player.play()

#func _on_Next_pressed():
#	scenario_player.play(current_node)

#func _on_Stop_pressed():
#	scenario_player.stop()

