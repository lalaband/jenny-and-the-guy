tool
extends Node

const StartScenario = preload("../resources/start_scenario.gd")
const StopScenario = preload("../resources/stop_scenario.gd")
const EveryIteration = preload("../resources/every_iteration.gd")

signal next(port)
signal finished()

func interpret(node):
	if node is StartScenario:
		_next()
	if node is EveryIteration:
		_next()
	if node is StopScenario:
		_finished()

func _next(port = 0):
	emit_signal("next", port)

func _finished():
	emit_signal("finished")
