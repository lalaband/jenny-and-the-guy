extends Node

export(Resource) var scenario
export(NodePath) var Interpreter
export(bool) var autoplay = false

var interpreter
var current_node

signal finished()

var in_every_iteration = false
var node_before_iteration = null

enum State {STOPED, PLAYING, WAIT_NEXT}
var state = STOPED

func _ready():
	interpreter = get_node(Interpreter)
	interpreter.connect("next", self, "_next")
	interpreter.connect("finished", self, "_finished")
	
	current_node = scenario.start_scenario_node
	
	if autoplay:
		play()

func play(node = null):
	assert scenario != null
	assert interpreter != null

	_play_node(current_node if node == null else node)

func stop(reset = false):
	state = STOPED
	
	if reset:
		current_node = scenario.start_scenario_node

func _next(port):
	if state != WAIT_NEXT:
		printerr("don't wait for next respond")
		return

	state = PLAYING

	var next_node = null
	
	if scenario.out_edges[current_node].has(port):
		next_node = scenario.out_edges[current_node][port].to

	if not in_every_iteration:
		in_every_iteration = true
		node_before_iteration = next_node
		_play_node(scenario.every_iteration_node)
		return

	if in_every_iteration and next_node == null:
		in_every_iteration = false
		_play_node(node_before_iteration)
		return

	_play_node(next_node)

func _finished():
	if state == STOPED:
		return
	
	state = STOPED
	emit_signal("finished")

func is_playing():
	return state != STOPED

func _play_node(node):
	if node == null:
		state = STOPED
		printerr("can't play null node")
		return

	current_node = node

	state = WAIT_NEXT

	interpreter.interpret(node)
